public class Account {
    String accountNumber;
    String accountType;
    String givenName;
    String familyName;

    public Account(String accountNumber, String accountType, String givenName, String familyName) {
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.givenName = givenName;
        this.familyName = familyName;
    }

    public String getAccountNumber(){
        return accountNumber;
    }

    public String getAccountType(){
        return accountType;
    }

    public String getGivenName(){
        return givenName;
    }
    public String getFamilyName(){
        return familyName;
    }
}
