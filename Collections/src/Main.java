//import com.sun.tools.corba.se.idl.ExceptionEntry;

import java.util.TreeMap;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;
import java.io.*;
import java.util.*;



public class Main {

    public static void main(String[] args) throws IOException {

        // Create a File object that refers to a local text file.
        File file = new File("/Users/libertypawn/Documents/SCHOOL/Winter 2018/360/cit-360/rawData.csv");


        TreeMap<String, Account> tmap =
                new TreeMap<String, Account>();
        FileReader fr= new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        String splitBy = ",";

        try {

            br = new BufferedReader(new FileReader(file));
            br.readLine();
            while ((line = br.readLine()) != null) {

                String[] collect = line.split(splitBy);

                Account data = new Account(collect[0], collect[1], collect[2], collect[3]);

                if (!tmap.containsKey(collect[0])) {
                    tmap.put(collect [0], data);

                }
            }
        }

        finally {
           br.close();
           fr.close();
        }

        Set<String> keys = tmap.keySet();

        for (String key: keys){
            Account account = (Account) tmap.get(key);
            System.out.println("Account number - " + account.getAccountNumber());
            System.out.println("Account type - " + account.getAccountType());
            System.out.println("Given Name - " + account.getGivenName());
            System.out.println("Family Name - " + account.getFamilyName());
            System.out.println("\n");
        }





    }




}
