

public class DogBean extends MammalBean {
    private String breed;
    private String name ;


    //getter and setter

    public String getbreed() {return breed;}
    public void setbreed(String breed) {this.breed = breed;}
    public String getname() {return name;}
    public void setname(String name) {this.name = name;}

    //constructor

    public DogBean(int legCount, String color, double height, String breed, String name){
        super(legCount, color, height);
        this.breed=breed;
        this.name=name;}

    //to string for dog bean

    public String toString(){
        return " legCount = " + getlegCount() +
                " color = " + getcolor() +
                " height = " + getheight() +
                " breed = " + getbreed() +
                " name = " + getname();
    }
}