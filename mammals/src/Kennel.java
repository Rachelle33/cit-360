public class Kennel {

    //Methods

    public DogBean[] buildDogs () {
        DogBean dog1 = new DogBean(4, "black", 162, "poodle", "Bob" );
        DogBean dog2 = new DogBean(4,"brown", 100, "lab", "Julie");
        DogBean dog3 = new DogBean(4,"white", 150, "dalmation", "Parker");
        DogBean dog4 = new DogBean (4, "White", 135, "greyhound", "Liz");
        DogBean dog5 = new DogBean(4, "Tan", 144, "borderCollie", "Ben");
        DogBean [] arrayODogs = {dog1, dog2, dog3, dog4, dog5 };
        return arrayODogs;
    }

//displayDogs method
    public void displayDogs (DogBean[] DogArray) {
        for (int i = 0; i<5; i++) {
            System.out.println(DogArray[i].toString());

        }
    }
}
