public class Main {

    public static void main(String[] args) {
        Kennel Kennel = new Kennel();
        DogBean[] dogBeanArray = Kennel.buildDogs();
        Kennel.displayDogs(dogBeanArray);
    }
}
