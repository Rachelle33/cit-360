public class DogBean extends MammelBean {
    private String breed;
    private String name;

    DogBean(String breed, String name, int legCount, String color, double height) {
        super(legCount, color, height);
        this.breed = breed;
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed() {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName() {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Leg Count = " + getLegCount() + "\nName = " + getName() + "\nColor = " + getColor() + "\nBreed = " + getBreed() + "\nHeight = " + getHeight() + "\n";
    }
}

