public class Kennel {
    public DogBean[] buildDogs() {
        DogBean dogArray[] = new DogBean[5];
        dogArray[0] = new DogBean ("Lab", " Tim",  4, " Black", 32);
        dogArray[1] = new DogBean ("Boxer", " Bob",  4, " Brown and White", 27);
        dogArray[2] = new DogBean ("Pug", " Frank",  4, " Black", 3);
        dogArray[3] = new DogBean ("Pitbull", " John",  4, " Gray", 28);
        dogArray[4] = new DogBean ("Lab", " Lucy",  3, " Yellow", 32);
        return dogArray;
    }

    public void displayDogs(DogBean[] dogsArray) {
        for (int i = 0; i < 5; i++) {
            System.out.println(dogsArray[i].toString());
        }
    }

}