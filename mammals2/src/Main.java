public class Main {

    public static void main(String[] args) {
        Kennel k1 = new Kennel();
        DogBean[] dogArray = k1.buildDogs();
        k1.displayDogs(dogArray);
    }
}