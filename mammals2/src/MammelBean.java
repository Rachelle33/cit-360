public class MammelBean {
    private int legCount;
    private String color;
    private double height;

    MammelBean(int legCount, String color, double height) {
        this.legCount = legCount;
        this.color = color;
        this.height = height;
    }

    public int getLegCount() {
        return legCount;
    }

    public String getColor() {
        return color;
    }

    public double getHeight() {
        return height;
    }

    public void setLegCount (int legCount) {
        this.legCount = legCount;
    }


    public void setColor (String color) {
        this.color = color;
    }

    public void setHeight (double height) {
        this.height = height;
    }

}


