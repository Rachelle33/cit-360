import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.*;

// Test MammalBean and DogBeans
public class UnitTest {


    // Create five MammalBean objects
    MammelBean m1 = new MammelBean(3, "Brown", 32);
    MammelBean m2 = new MammelBean(4, "Yellow", 24);
    MammelBean m3 = new MammelBean(4, "White", 36);
    MammelBean m4 = new MammelBean(4, "Black", 30);
    MammelBean m5 = new MammelBean(4, "Gray", 45);

    // Create five DogBean objects
    DogBean d1 = new DogBean ("Lab", "Tim",  m1.getLegCount(), m1.getColor(), m1.getHeight());
    DogBean d2 = new DogBean ("Boxer", "Bob",  m2.getLegCount(), m2.getColor(),m2.getHeight());
    DogBean d3 = new DogBean ("Pug", "Frank",  m3.getLegCount(), m3.getColor(), m3.getHeight());
    DogBean d4 = new DogBean ("Pitbull", "John",  m4.getLegCount(), m4.getColor(), m4.getHeight());
    DogBean d5 = new DogBean ("Greyhound", "Lucy",  m5.getLegCount(), m5.getColor(), m5.getHeight());

    // Create a test for each attribute in MamnalBean and make sure they pass
    @Test
    public void testLegCount () {
        assertEquals(3, m1.getLegCount());
        assertEquals(4, m2.getLegCount());
        assertEquals(4, m3.getLegCount());
        assertEquals(4, m4.getLegCount());
        assertEquals(4, m5.getLegCount());

    }

    @Test
    public void testColor () {
        assertEquals("Brown", m1.getColor());
        assertEquals("Yellow", m2.getColor());
        assertEquals("White", m3.getColor());
        assertEquals("Black", m4.getColor());
        assertEquals("Gray", m5.getColor());
    }

    @Test
    public void testHeight () {
        double expected = 32;
        double delta = expected * .0001;
        assertEquals(32, m1.getHeight(), delta);
        assertEquals(24, m2.getHeight(), delta);
        assertEquals(36, m3.getHeight(), delta);
        assertEquals(30, m4.getHeight(), delta);
        assertEquals(45, m5.getHeight(), delta);
    }

    // Create tests for the two attributes in DogBean not carried over from MammalBean
    @Test
    public void testBreed () {
        assertEquals("Lab", d1.getBreed());
        assertEquals("Boxer", d2.getBreed());
        assertEquals("Pug", d3.getBreed());
        assertEquals("Pitbull", d4.getBreed());
        assertEquals("Greyhound", d5.getBreed());
    }

    @Test
    public void testName () {
        assertEquals("Tim", d1.getName());
        assertEquals("Bob", d2.getName());
        assertEquals("Frank", d3.getName());
        assertEquals("John", d4.getName());
        assertEquals("Lucy", d5.getName());
    }

    // Create a set containing MammalBean objects and run a test to make sure the objects are in the set
    @Test
    public void hashSetMammal() {
        HashSet<MammelBean> setA = new HashSet<MammelBean>();
        setA.add(m1);
        setA.add(m2);
        setA.add(m3);
        setA.add(m4);
        setA.add(m5);
        setA.contains(m1);
        setA.contains(m2);
        setA.contains(m3);
        setA.contains(m4);
        setA.contains(m5);
        setA.remove(m3);
        assertTrue(setA.contains(m1));
        assertTrue(setA.contains(m2));
        assertFalse(setA.contains(m3));
        assertTrue(setA.contains(m4));
        assertTrue(setA.contains(m5));
    }

    // Create a treeMap that has DogBean objects and make sure the Keys and Values are in the treeMap and they pass the test
    @Test
    public void treeMapDogBean () {
        TreeMap<String, DogBean> tmap = new TreeMap<String, DogBean>();
        tmap.put("Tim", d1);
        tmap.put("Bob", d2);
        tmap.put("Frank", d3);
        tmap.put("John", d4);
        tmap.put("Lucy", d5);
        tmap.containsKey("Tim");
        tmap.containsKey("Bob");
        tmap.containsKey("Frank");
        tmap.containsKey("John");
        tmap.containsKey("Lucy");
        tmap.containsValue(d1);
        tmap.containsValue(d2);
        tmap.containsValue(d3);
        tmap.containsValue(d4);
        tmap.containsValue(d5);
        tmap.remove("Frank", d3);
        assertTrue(tmap.containsKey("Tim"));
        assertTrue(tmap.containsKey("Bob"));
        assertFalse(tmap.containsKey("Frank"));
        assertTrue(tmap.containsKey("John"));
        assertTrue(tmap.containsKey("Lucy"));
        assertTrue(tmap.containsValue(d1));
        assertTrue(tmap.containsValue(d2));
        assertFalse(tmap.containsValue(d3));
        assertTrue(tmap.containsValue(d4));
        assertTrue(tmap.containsValue(d5));
    }
}
